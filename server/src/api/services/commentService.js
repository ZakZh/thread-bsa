import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const getCommentById = id => commentRepository.getCommentById(id);

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const update = async (userId, { id, body }) => {
  const comment = await getCommentById(id);
  const updated = await commentRepository.updateById(id, { body });
  return userId === comment.userId ? updated : comment;
};

export const deleteComment = async (userId, request) => {
  let { id } = request;
  const comment = await getCommentById(id);
  function deleteRequest() {
    const deletedComment = commentRepository.updateById(id, { deleted: true }) ? { id } : false;
    id = deletedComment.id;
    return { id };
  }

  return (userId === comment.userId) ? deleteRequest() : {};
};

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : commentReactionRepository.getCommentReaction(userId, commentId);
};

