import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const update = async (userId, { id, body, imageId }) => {
  const post = await getPostById(id);
  const updated = await postRepository.updateById(id, { body, imageId });
  return userId === post.userId ? updated : post;
};

export const deletePost = async (userId, request) => {
  let { id } = request;
  const post = await getPostById(id);

  function deleteRequest() {
    const deletedPost = postRepository.updateById(id, { deleted: true }) ? { id } : false;
    id = deletedPost.id;
    return { id };
  }
  return (userId === post.userId) ? deleteRequest() : {};
};

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
