import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    const likeCommentCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: { id, deleted: false },
      rejectOnEmpty: true,
      required: false,
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCommentCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCommentCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: []
        // duplicating: false
      }
      ]
    });
  }
}

export default new CommentRepository(CommentModel);
