import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import CardSubMenu from '../CardSubMenu';

import styles from './styles.module.scss';

const Post = ({
  userId,
  post,
  deletePost,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  sharePost
}) => {
  const {
    id,
    image,
    body,
    user,
    userId: postUserId,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt

  } = post;
  const date = moment(createdAt).fromNow();

  const cardSubMenuActions = [
    { name: 'Update', func: () => toggleExpandedUpdatePost(id) },
    { name: 'Delete', func: () => deletePost({ id }) }
  ];

  let actionsDropdown;
  if (userId === postUserId) {
    actionsDropdown = <CardSubMenu id={id} cardSubMenuActions={cardSubMenuActions} />;
  }

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {user.username}
            {' - '}
            {date}
          </span>
          { actionsDropdown }
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => likePost(id)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>

        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => dislikePost(id)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>

        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => toggleExpandedPost(id)}
        >
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  userId: PropTypes.string,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

Post.defaultProps = {
  userId: undefined
};

export default Post;
