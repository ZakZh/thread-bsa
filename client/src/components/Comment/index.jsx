import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import CardSubMenu from '../CardSubMenu';
import UpdateComment from '../UpdateComment';

import styles from './styles.module.scss';

const Comment = ({
  userId,
  comment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  toggleExpandedUpdateCommentForm,
  expandedUpdateComment
}) => {
  const { body, createdAt, user, userId: createrId, id: commentId, postId, likeCount, dislikeCount } = comment;

  const cardSubMenuActions = [
    { name: 'Update', func: () => { toggleExpandedUpdateCommentForm(commentId); } },
    { name: 'Delete', func: () => { deleteComment({ id: commentId, postId }); } }
  ];

  let actionsDropdown;
  if (userId === createrId) {
    actionsDropdown = <CardSubMenu id={commentId} cardSubMenuActions={cardSubMenuActions} />;
  }

  const updateForm = (
    <UpdateComment
      comment={comment}
      updateComment={updateComment}
      toggleExpandedUpdateCommentForm={toggleExpandedUpdateCommentForm}
    />
  );

  const commentBody = (
    <CommentUI.Text>
      {body}
    </CommentUI.Text>
  );

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => { likeComment(commentId); }}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>

        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => { dislikeComment(commentId); }}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        {actionsDropdown}
        {expandedUpdateComment && expandedUpdateComment.id === commentId ? updateForm : commentBody}
      </CommentUI.Content>

    </CommentUI>
  );
};

Comment.propTypes = {
  userId: PropTypes.string.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  toggleExpandedUpdateCommentForm: PropTypes.func.isRequired,
  expandedUpdateComment: PropTypes.objectOf(PropTypes.any)
};

Comment.defaultProps = {
  expandedUpdateComment: undefined
};

export default Comment;
