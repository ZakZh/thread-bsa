import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'semantic-ui-react';

const FilterPostDropdown = ({
  filterPostOptions,
  postFiltering
}) => (
  <Dropdown
    options={filterPostOptions}
    onChange={(el, data) => postFiltering(data.value)}
    defaultValue={filterPostOptions[0].value}
    selection
  />
);

FilterPostDropdown.propTypes = {
  filterPostOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  postFiltering: PropTypes.func.isRequired
};

FilterPostDropdown.defaultProps = {

};

export default FilterPostDropdown;
