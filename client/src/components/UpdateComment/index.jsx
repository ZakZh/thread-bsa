import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';

const UpdateComment = ({
  comment,
  updateComment,
  toggleExpandedUpdateCommentForm
}) => {
  const { id, body: commentBody, postId } = comment;
  const [body, setBody] = useState(commentBody);

  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    await updateComment({ id, body, postId });
    setBody('');
    toggleExpandedUpdateCommentForm();
  };

  return (comment
    ? (
      <Form reply onSubmit={handleUpdateComment}>
        <Form.TextArea
          value={body}
          placeholder="Type a comment..."
          onChange={ev => setBody(ev.target.value)}
        />
        <Button type="submit" content="Update comment" labelPosition="left" icon="edit" primary />
      </Form>
    ) : (
      <Spinner />
    ));
};

UpdateComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  toggleExpandedUpdateCommentForm: PropTypes.func.isRequired
};

export default UpdateComment;
