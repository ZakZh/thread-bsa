import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'semantic-ui-react';
import { v4 as uuidv4 } from 'uuid';

import styles from './styles.module.scss';

const CardSubMenu = ({
  id,
  cardSubMenuActions: actions
}) => (
  <Dropdown
    style={{ float: 'right' }}
    direction="left"
    className={styles.dropdown}
    icon="chevron down"
  >
    <Dropdown.Menu>
      {actions.map(action => (
        <Dropdown.Item
          key={`${id}_${uuidv4()}`}
          text={action.name}
          onClick={action.func}
        />
      ))}
    </Dropdown.Menu>
  </Dropdown>
);
CardSubMenu.propTypes = {
  id: PropTypes.string.isRequired,
  cardSubMenuActions: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default CardSubMenu;
