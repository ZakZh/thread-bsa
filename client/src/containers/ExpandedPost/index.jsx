import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  toggleExpandedUpdateCommentForm
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  uploadImage,
  likePost: like,
  dislikePost: dislike,
  deletePost: deleteCurrentPost,
  toggleExpandedPost: toggle,
  toggleExpandedUpdatePost: toggleUpdatePost,
  toggleExpandedUpdateCommentForm: toggleUpdateCommentForm,
  expandedUpdateComment,
  addComment: add,
  updateComment: updtComment,
  deleteComment: dltComment,
  likeComment: lkComment,
  dislikeComment: dlkComment
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post ? (
      <Modal.Content>
        <Post
          userId={userId}
          post={post}
          deletePost={deleteCurrentPost}
          likePost={like}
          dislikePost={dislike}
          toggleExpandedPost={toggle}
          uploadImage={uploadImage}
          toggleExpandedUpdatePost={toggleUpdatePost}
          sharePost={sharePost}
        />
        <CommentUI.Group style={{ maxWidth: '100%' }}>
          <Header as="h3" dividing>
            Comments
          </Header>
          {post.comments && post.comments.sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt)).map(comment => (
            <Comment
              post={post}
              userId={userId}
              key={comment.id}
              comment={comment}
              updateComment={updtComment}
              deleteComment={dltComment}
              likeComment={lkComment}
              dislikeComment={dlkComment}
              toggleExpandedUpdateCommentForm={toggleUpdateCommentForm}
              expandedUpdateComment={expandedUpdateComment}
            />
          ))}
          <AddComment postId={post.id} addComment={add} />
        </CommentUI.Group>
      </Modal.Content>
    ) : (
      <Spinner />
    )}
  </Modal>

);

ExpandedPost.propTypes = {
  userId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  toggleExpandedUpdateCommentForm: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  expandedUpdateComment: PropTypes.objectOf(PropTypes.any),
  sharePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  expandedUpdateComment: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  expandedUpdateComment: rootState.posts.expandedUpdateComment
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  toggleExpandedUpdateCommentForm
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedPost);
