import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  UPDATE_POST,
  SET_EXPANDED_UPDATE_POST,
  SET_EXPANDED_UPDATE_COMMENT,
  DELETE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = (posts, post) => ({
  type: UPDATE_POST,
  posts,
  post
});

const setExpandedUpdatePostAction = post => ({
  type: SET_EXPANDED_UPDATE_POST,
  post
});

const deletePostAction = posts => ({
  type: DELETE_POST,
  posts
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setExpandedUpdateCommentFormAction = comment => ({
  type: SET_EXPANDED_UPDATE_COMMENT,
  comment
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = updatedPost => async (dispatch, getRootState) => {
  const { id, body, imageId } = await postService.updatePost(updatedPost);
  let newPost;

  const mapPost = post => ({
    ...post,
    body,
    imageId
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => {
    if (post.id !== id) {
      return post;
    }

    newPost = mapPost(post);
    return newPost;
  });

  const post = expandedPost ? newPost : undefined;

  dispatch(updatePostAction(updated, post));
};

export const deletePost = deletedPost => async (dispatch, getRootState) => {
  const { id } = await postService.deletePost(deletedPost);
  const {
    posts: { posts }
  } = getRootState();

  const deleted = posts.filter(post => (post.id !== id));

  dispatch(deletePostAction(deleted));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const toggleExpandedUpdatePost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedUpdatePostAction(post));
};

export const toggleExpandedUpdateCommentForm = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setExpandedUpdateCommentFormAction(comment));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.likePost(postId);

  let diff;
  if (id && updatedAt === createdAt) {
    diff = [1, 0];
  } else if (id && updatedAt !== createdAt) {
    diff = [1, -1];
  } else {
    diff = [-1, 0];
  }

  // if ID exists then the post was liked, otherwise - like was removed
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff[0], // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + diff[1] // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.dislikePost(postId);

  let diff;
  if (id && updatedAt === createdAt) {
    diff = [1, 0];
  } else if (id && updatedAt !== createdAt) {
    diff = [1, -1];
  } else {
    diff = [-1, 0];
  }

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff[0], // diff is taken from the current closure
    likeCount: Number(post.likeCount) + diff[1] // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = request => async (dispatch, getRootState) => {
  const { id, body } = await commentService.updateComment(request);

  const mapComments = post => {
    const { comments } = post;
    const mapComment = comment => ({
      ...comment,
      body
    });
    const updatedComments = comments.map(comment => (comment.id !== id ? comment : mapComment(comment)));

    const updatedPost = {
      ...post,
      comments: updatedComments
    };

    return updatedPost;
  };

  const {
    posts: { expandedPost }
  } = getRootState();

  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const deleteComment = request => async (dispatch, getRootState) => {
  const { id, postId } = request;
  const deleted = await commentService.deleteComment({ id });

  const mapComments = post => {
    const { comments } = post;
    let updatedComments;
    if (comments) {
      updatedComments = comments.filter(comment => (comment.id !== deleted.id));
    }

    const updatedPost = {
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: updatedComments
    };

    return updatedPost;
  };

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.likeComment(commentId);

  let diff;
  if (id && updatedAt === createdAt) {
    diff = [1, 0];
  } else if (id && updatedAt !== createdAt) {
    diff = [1, -1];
  } else {
    diff = [-1, 0];
  }

  const mapComments = post => {
    const { comments } = post;
    let updatedComments;
    if (comments) {
      updatedComments = comments.map(comment => {
        if (comment.id !== commentId) {
          return comment;
        }
        return {
          ...comment,
          likeCount: Number(comment.likeCount) + diff[0], // diff is taken from the current closure
          dislikeCount: Number(comment.dislikeCount) + diff[1] // diff is taken from the current closure
        };
      });
    }

    const updatedPost = {
      ...post,
      comments: updatedComments
    };

    return updatedPost;
  };

  const {
    posts: { expandedPost }
  } = getRootState();

  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.dislikeComment(commentId);

  let diff;
  if (id && updatedAt === createdAt) {
    diff = [1, 0];
  } else if (id && updatedAt !== createdAt) {
    diff = [1, -1];
  } else {
    diff = [-1, 0];
  }

  const mapComments = post => {
    const { comments } = post;
    let updatedComments;
    if (comments) {
      updatedComments = comments.map(comment => {
        if (comment.id !== commentId) {
          return comment;
        }
        return {
          ...comment,
          dislikeCount: Number(comment.dislikeCount) + diff[0], // diff is taken from the current closure
          likeCount: Number(comment.likeCount) + diff[1] // diff is taken from the current closure
        };
      });
    }

    const updatedPost = {
      ...post,
      comments: updatedComments
    };

    return updatedPost;
  };

  const {
    posts: { expandedPost }
  } = getRootState();

  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};
