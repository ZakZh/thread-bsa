import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  SET_EXPANDED_POST,
  SET_EXPANDED_UPDATE_POST,
  SET_EXPANDED_UPDATE_COMMENT
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case UPDATE_POST:
      return {
        ...state,
        posts: action.posts,
        expandedPost: action.post
      };
    case DELETE_POST:
      return {
        ...state,
        posts: action.posts,
        expandedPost: undefined
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SET_EXPANDED_UPDATE_POST:
      return {
        ...state,
        expandedUpdatePost: action.post
      };
    case SET_EXPANDED_UPDATE_COMMENT:
      return {
        ...state,
        expandedUpdateComment: action.comment
      };
    default:
      return state;
  }
};
