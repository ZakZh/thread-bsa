/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import ExpandedUpdatePost from 'src/containers/ExpandedUpdatePost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import FilterPostDropdown from 'src/components/FilterPostDropDown';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  addPost,
  updatePost,
  deletePost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  other: false,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  expandedUpdatePost,
  hasMorePosts,
  addPost: createPost,
  updatePost: readdPost,
  deletePost: deleteCurrentPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  toggleExpandedUpdatePost: toggleUpdatePost
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);

  const filterPostOptions = [
    {
      key: 'All posts',
      text: 'All Post',
      value: 'all'
    },
    {
      key: 'Own posts',
      text: 'My Posts',
      value: 'own'
    },
    {
      key: 'Other posts',
      text: 'Only other posts',
      value: 'other'
    },
    {
      key: 'Liked posts',
      text: 'Liked',
      value: 'liked'
    }
  ];

  const postFiltering = filter => {
    switch (filter) {
      case 'all':
        postsFilter.userId = undefined;
        postsFilter.other = undefined;
        break;
      case 'own':
        postsFilter.userId = userId;
        postsFilter.other = undefined;
        break;
      case 'other':
        postsFilter.userId = userId;
        postsFilter.other = 'other';
        break;
      case 'liked':
        postsFilter.userId = userId;
        postsFilter.other = 'liked';
        break;
      default:
        postsFilter.userId = undefined;
        postsFilter.other = false;
        break;
    }

    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);
  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <FilterPostDropdown filterPostOptions={filterPostOptions} postFiltering={postFiltering} />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            userId={userId}
            post={post}
            deletePost={deleteCurrentPost}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            uploadImage={uploadImage}
            toggleExpandedUpdatePost={toggleUpdatePost}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost
        && (
          <ExpandedPost
            userId={userId}
            deletePost={deleteCurrentPost}
            sharePost={sharePost}
            uploadImage={uploadImage}
            toggleExpandedPost={toggle}
            toggleExpandedUpdatePost={toggleUpdatePost}
          />
        )}
      {expandedUpdatePost && (
        <ExpandedUpdatePost
          uploadImage={uploadImage}
          updatePost={readdPost}
          toggleExpandedUpdatePost={toggleUpdatePost}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  expandedUpdatePost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  expandedUpdatePost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  expandedUpdatePost: rootState.posts.expandedUpdatePost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  addPost,
  updatePost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Thread);
