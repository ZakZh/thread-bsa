import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
// import moment from 'moment';
import {
  toggleExpandedUpdatePost
} from 'src/containers/Thread/actions';
import UpdatePost from 'src/components/UpdatePost';
import Spinner from 'src/components/Spinner';
// import styles from './styles.module.scss';

const ExpandedUpdatePost = ({
  post,
  toggleExpandedUpdatePost: toggleUpdatePost,
  updatePost,
  uploadImage
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggleUpdatePost()}>
    {post ? (
      <Modal.Content>
        <UpdatePost
          post={post}
          toggleExpandedUpdatePost={toggleUpdatePost}
          updatePost={updatePost}
          uploadImage={uploadImage}
        />
      </Modal.Content>
    ) : (
      <Spinner />
    )}
  </Modal>
);

ExpandedUpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedUpdatePost
});

const actions = { toggleExpandedUpdatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedUpdatePost);
